<?php
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\CategoryRepository;
use App\Category;
use Faker\Factory as Faker;

class CategoryTest extends TestCase
{
    protected $category;

    public function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();
        // chuẩn bị dữ liệu test
        $this->category = [
            'name' => $this->faker->name,
            'description' => $this->faker->name,
        ];
        // khởi tạo lớp CategoryRepository
        $this->categoryRepository = new CategoryRepository();
    }

    public function testStore()
    {
        // Gọi hàm tạo
        $category = $this->categoryRepository->storeCategory($this->category);
        // Kiểm tra xem kết quả trả về có là thể hiện của lớp Category hay không
        $this->assertInstanceOf(Category::class, $category);
        // Kiểm tra data trả về
        $this->assertEquals($this->category['name'], $category->name);
        $this->assertEquals($this->category['description'], $category->description);
        // Kiểm tra dữ liệu có tồn tại trong cơ sở dữ liệu hay không
        $this->assertDatabaseHas('categories', $this->category);
    }
    public function testShow()
    {
        $category = new Category;
        $category->name = 'awdawddq3wreqe';
        $category->description = 'q2eq2eq2e';
        $category->save();
        $found = $this->categoryRepository->showCategory($category->id);
        $this->assertInstanceOf(Category::class, $found);
        $this->assertEquals($found->name, $category->name);
        $this->assertEquals($found->description, $category->description);
    }
    public function testDestroy()
    {
        $category = new Category;
        $category->name = 'awdawdd';
        $category->description = 'awdawdd';
        $category->save();
        $deleteCategory = $this->categoryRepository->destroyCategory($category);
        // Kiểm tra dữ liệu có trả về true hay không
        $this->assertTrue($deleteCategory);
        // kiểm tra xem dữ liệu đã được xóa trong cơ sở dữ liệu hay chưa
        $this->assertDatabaseMissing('categories', $category->toArray());
    }
    public function testUpdate()
    {
        $category = new Category;
        $category->name = 'awdawddw';
        $category->description = 'awdawddw';
        $category->save();
        $newCategory = $this->categoryRepository->updateCategory($this->category, $category);
        // Kiem tra data tra ve
        $this->assertInstanceOf(Category::class, $newCategory);
        $this->assertEquals($newCategory->name, $this->category['name']);
        $this->assertEquals($newCategory->description, $this->category['description']);
        // Kiểm tra xem cơ sở dữ liệu đã được cập nhập hay chưa
        $this->assertDatabaseHas('categories', $this->category);
    }
}

