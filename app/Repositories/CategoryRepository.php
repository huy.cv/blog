<?php

namespace App\Repositories;

use App\Category;

class CategoryRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = app()->make(Category::class); // app()->make khoi tao 1 calss
    }

    public function storeCategory($data) : Category
    {
        $category = $this->model->create($data);
        return $category;
    }

    public function updateCategory($data, $category)
    {
        $categorByUpdate = Category::find($category->id);
        $categorByUpdate->name = $data['name'];
        $categorByUpdate->description = $data['description'];
        $categorByUpdate->update();
        return $categorByUpdate;
    }

    public function showCategory($category_id) : Category
    {
        return $this->model->findOrFail($category_id);
    }

    public function destroyCategory($category)
    {
        $cat = Category::find($category->id);
        return $cat->delete();
    }
}

